FROM alpine
MAINTAINER khazov@iac.spb.ru

ENV RT_VERSION 4.4.4


RUN \
    apk --update upgrade && \
    apk add rt4 \
	    opensmtpd \
	    supervisor \
	    iproute2 \
	    nginx

#	    spawn-fcgi \
ADD ./etc/nginx.conf /etc/nginx/conf.d/
ADD ./etc/supervisor.ini /etc/supervisor.d/
ADD ./etc/RT_SiteConfig.pm /etc/rt4/

#Configure opensmtpd
ADD ./etc/smtpd.conf /etc/smtpd/
ADD ./etc/aliases /etc/smtpd/
RUN  /usr/sbin/newaliases

# Add cron job for full text search indexer
RUN echo "/bin/sh" > /etc/periodic/daily/rt-fulltext-indexer && \
    echo "/usr/sbin/rt-fulltext-indexer" >> /etc/periodic/daily/rt-fulltext-indexer && \
    chmod a+x /etc/periodic/daily/rt-fulltext-indexer

# Finish RUN
RUN rm -rf /tmp/* /var/tmp/*

EXPOSE 25 
EXPOSE 8080

#CMD spawn-fcgi -u rt4 -g rt4 -p 8080 -P /var/run/rt.pid -- /usr/sbin/rt-server.fcgi
CMD ["/usr/bin/supervisord"]
