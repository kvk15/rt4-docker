use utf8;

# Any configuration directives you include  here will override
# RT's default configuration file, RT_Config.pm
#
# To include a directive here, just copy the equivalent statement
# from RT_Config.pm and change the value. We've included a single
# sample value below.
#
# If this file includes non-ASCII characters, it must be encoded in
# UTF-8.
#
# This file is actually a perl module, so you can include valid
# perl code, as well.
#
# The converse is also true, if this file isn't valid perl, you're
# going to run into trouble. To check your SiteConfig file, use
# this command:
#
#   perl -c /path/to/your/etc/RT_SiteConfig.pm
#
# You must restart your webserver after making changes to this file.
#

# You may also split settings into separate files under the etc/RT_SiteConfig.d/
# directory.  All files ending in ".pm" will be parsed, in alphabetical order,
# after this file is loaded.


Set($Timezone, "Europe/Moscow");

Set($LogToSyslog, "info");
Set($MaxAttachmentSize, 30*1024*1024);  # 30MiB
Set($AutocompleteOwners, 1);
Set(@EmailInputEncodings, qw(utf-8 koi8-r cp-1251 iso-8859-1 us-ascii));


# You must install Plugins on your own, this is only an example
# of the correct syntax to use when activating them:
#     Plugin( "RT::Authen::ExternalAuth" );

Set( $CommentAddress, '' );
Set( $CorrespondAddress, '' );

Set( $DatabaseHost, '172.16.1.1' );
Set( $DatabaseName, 'rt4' );
Set( $DatabasePassword, 'qwe123' );
Set( $DatabasePort, '' );
Set( $DatabaseType, 'Pg' );
Set( $DatabaseUser, 'rt' );
Set( $DatabaseAdmin, "postgres");

Set( $Organization, 'tick.example.com' );
Set( $OwnerEmail, 'root@example.com' );
Set( $SendmailPath, '/usr/sbin/sendmail' );
Set( $WebDomain, 'example.com' );
Set( $WebPort, '8080' );
Set( $rtname, 'Ticket System Organisation Name' );
1;
